# do not produce empty debuginfo package
%global debug_package %{nil}

Summary: Dummy package for testing an onboarding purposes
Name: redhat-internal-test-package
Version: 0.2.0
Release: 15%{?dist}
License: GPLv2+
URL: https://github.com/packit/redhat-internal-test-package
Group: Applications/File
Source0: https://github.com/packit/redhat-internal-test-package/archive/refs/tags/%{version}.tar.gz
Source1: README
Source2: LICENSE

BuildArch: noarch
BuildRequires: python3-devel
BuildRequires: pyproject-rpm-macros

%description
Dummy package that is meant to be used for testing or onboarding purposes,
but which should not ever get to a real release.

%prep
%autosetup -n %{name}-%{version}

cp %{SOURCE1} .
cp %{SOURCE2} .

%generate_buildrequires
%pyproject_buildrequires

%build
%pyproject_wheel

%install
%pyproject_install

%files
%doc README LICENSE
%{_bindir}/hecker
%{python3_sitelib}/*

%changelog
* Wed Mar 01 2023 Jan Kaluza <jkaluza@redhat.com> - 0.2.0-12
- Testing RHELX workflow at 2022-09-23
  Resolves: RHELX-73

* Wed Mar 01 2023 Jan Kaluza <jkaluza@redhat.com> - 0.2.0-11
- Testing RHELX workflow at 2022-09-23
  Resolves: RHELX-73

* Fri Sep 23 2022 Honza Horak <hhorak@redhat.com> - 0.2.0-10
- Testing RHELX workflow at 2022-09-23
  Resolves: RHELX-73

* Sun Sep 18 2022 Honza Horak <hhorak@redhat.com> - 0.2.0-9
- Testing workflow at 2022-09-18_19-45-04 through BZ#2127735

* Fri Sep 16 2022 Honza Horak <hhorak@redhat.com> - 0.2.0-8
- Testing workflow at 2022-09-16_16-15-59 through BZ#2127488

* Fri Aug 26 2022 Honza Horak <hhorak@redhat.com> - 0.2.0-7
- Testing workflow at 2022-08-26_16-17-08 through BZ#2121767

* Tue Jul 12 2022 Jiri Kyjovsky <jkyjovsk@redhat.com> - 0.1-5
- Add upstream repository for dummy test package

* Mon May 02 2022 Honza Horak <hhorak@redhat.com> - 0.1-4
- Add dummy entry

* Thu Mar 24 2022 Honza Horak <hhorak@redhat.com> - 0.1-3
- Onboarding hhorak

* Wed Mar 02 2022 Honza Horak <hhorak@redhat.com> - 0.1-2
- Release bump

* Tue Mar 01 2022 Honza Horak <hhorak@redhat.com> - 0.1-1
- Initial packaging
